let express = require('express'),
    https = require('https'),
    http = require('http'),
    handlebars = require('express-handlebars'),
    cookieParser = require('cookie-parser'),
    compression = require('compression'),
    bodyParser = require('body-parser'),
    util = require('util'),
    winston = require('winston'),
    session = require('express-session'),
    uid = require('uid-safe'),
    flash = require('express-flash'),
    Redis = require('ioredis'),
    RedisStore = require('connect-redis')(session);

/** Controllers requires */
let home = require('./controllers/home');

/** Local modules requires */
let logger = require('./lib/logger');

/** Habitat Env Configuration */
let env = (function(){
  let Habitat = require("habitat");
  Habitat.load();
  return new Habitat();
}());

let app = express();

/** Express JS configuration */
app.use(cookieParser(env.get("SESSION_SECRET")));

var redis = new Redis({
  "sentinels": [
    { "host": env.get("REDIS_SENTINEL_NODE_1_HOST"), "port": env.get("REDIS_SENTINEL_NODE_1_PORT") },
    { "host": env.get("REDIS_SENTINEL_NODE_1_HOST"), "port": env.get("REDIS_SENTINEL_NODE_1_PORT") },
    { "host": env.get("REDIS_SENTINEL_NODE_1_HOST"), "port": env.get("REDIS_SENTINEL_NODE_1_PORT") }
  ],
  "name": env.get('REDIS_SENTINEL_CLUSTER_NAME')
});
let store = new RedisStore({"client": redis});

app.use(require('express-session')({
  name: env.get("COOKIE_NAME"),
  secret: env.get("SESSION_SECRET"),
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
    domain: env.get("COOKIE_DOMAIN")
  },
  store: store,
  resave: false,
  saveUninitialized: false,
  unset: "destroy"
}));

function unlessHealthcheck(fn) {
    return function(req, res, next) {
        if (req.path === '/healthcheck' && req.method === 'GET') {
            next();
        } else {
            fn(req, res, next);
        }
    }
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.disable('x-powered-by');
app.engine('handlebars', handlebars({
  defaultLayout: 'main',
  helpers: require("./public/helpers.js").helpers,
}));
app.set('view engine', 'handlebars');
app.use(express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/vendor'));
app.use(compression());
app.use(passport.initialize());
app.use(unlessHealthcheck(flash()));
// app.use(flash());

app.use('/', home.private);

/**
 * Page not found middleware
 * @name PageNotFound_middleware
 * @param {callback} middleware- Express middleware
 */
app.use(function (req, res, next) {
  // logger.error("ERROR: 404 - Page not found: ("+req.originalUrl+")");

  if(!req.xhr){
    res.render('error', {
      "layout": "empty",
      "headerTitle": "Error 404",
      "title": "Errore 404",
      "subtitle": "La pagina che stavi cercando non esiste."
    });
  }else{
    logger.error("Error 404 on XHR");
    res.json({"result": false});
  }

});

/**
 * Error 500 middleware
 * @name Error500_middleware
 * @param {callback} middleware- Express middleware
 */
app.use(function(err, req, res, next) {
  logger.error("ERROR: 500: ("+req.originalUrl+")");
  console.error(err.stack);
  // console.log(">> res.headersSent: " + res.headersSent);
  // console.log(JSON.stringify(err));
  if(!res.headersSent && err.code != "CERT_HAS_EXPIRED"){
    return res.render('error', {
      "layout": "empty",
      "headerTitle": "Error 500",
      "title": "Errore 500",
      "subtitle": "Ops. Qualcosa è andato storto. Contattare un amministratore."
    });
  }else{
    logger.error("Cannot send headers after they are sent. Probably an expired certificate during the LDAPS query to ICT.");
  }

});

http.createServer(app).listen(env.get("SERVER_PORT"));
