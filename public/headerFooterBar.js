/**
 * Libreria per gestire la barra superiore e inferiore.
 *
 * @author Roberto Belardo <roberto.belardo@rai.it>
 * @version 1.0.0
 */

var headerFooterBar = {};

// Private ---------------------------------------------------------------------
var $HeaderNavigationBar = $('#HeaderNavigationBar'),
    $HeaderNavigationBarContent = $('#HeaderNavigationBarContent'),
    $HeaderNavigationBarButtonInput = $('#HeaderNavigationBarButtonInput'),
    $HeaderNavigationBarButton = $('#HeaderNavigationBarButton'),
    $FooterNavigationBar = $('#FooterNavigationBar'),
    $FooterNavigationBarButtonInput = $('#FooterNavigationBarButtonInput'),
    $FooterNavigationBarButton = $('#FooterNavigationBarButton'),
    $FooterNavigationBarContent = $('#FooterNavigationBarContent');

/**
 * @name addButtonToFooterBar
 * @description It adds a button to the Footer Bar.
 * @function
 * @public
 *
 * @param {string} iconTitle - The icon title
 * @param {string} iconFontAwesomeClass - The icon FontAwesome css class
 * @param {string} iconDivID - The DOM <div> id where it has to attach the button
 *
 */
function addButtonToFooterBar(iconTitle, iconFontAwesomeClass, iconDivID) {
  return $FooterNavigationBarContent.append(
    "<div id='"+iconDivID+"' class='floatRight navBarIcon'>"+
      "<div>"+
        "<span class='fa-stack fa-lg'>"+
          "<i class='far fa-circle fa-stack-2x'></i>"+
          "<i class='fas "+iconFontAwesomeClass+" fa-stack-1x'></i>"+
        "</span>"+
      "</div>"+
      "<div class='navBarIconText'>"+iconTitle+"</div>"+
    "</div>");
}

/**
 * @name addButtonToHeaderBar
 * @description It adds a button to the Header Bar.
 * @function
 * @public
 *
 * @param {string} iconTitle - The icon title
 * @param {string} iconFontAwesomeClass - The icon FontAwesome css class
 * @param {string} iconDivID - The DOM <div> id where it has to attach the button
 *
 */
function addButtonToHeaderBar(iconTitle, iconFontAwesomeClass, iconDivID) {
  return $HeaderNavigationBarContent.append(
    "<div id='"+iconDivID+"' class='floatRight navBarIcon'>"+
      "<div>"+
        "<span class='fa-stack fa-lg'>"+
          "<i class='far fa-circle fa-stack-2x'></i>"+
          "<i class='fas "+iconFontAwesomeClass+" fa-stack-1x'></i>"+
        "</span>"+
      "</div>"+
      "<div class='navBarIconText'>"+iconTitle+"</div>"+
    "</div>");
}

/**
 * @name removeButtonFromBar
 * @description It adds a button to the Bar.
 * @function
 * @public
 * @param {string} iconDivID - The DOM <div> id where the button to remove is attached
 */
function removeButtonFromBar(iconDivID) {
  $("#"+iconDivID+"").remove();
}

/**
 * @name showHideHeaderFooterNavigationBar
 * @description Shows or hide the header and footer bars depending on their current state.
 * @function
 * @private
 */
function showHideHeaderFooterNavigationBar() {
  var headerFooterBarVisible = sessionStorage.getItem("headerFooterBarVisible") || "false";

  if (headerFooterBarVisible === "false") {
    showBar();
  }else{
    hideBar();
  }
}

/**
 * @name hideBar
 * @description It hides the header and the footer simoultanesly.
 * @function
 * @public
 */
function hideBar() {
  var headerFooterBarVisible = sessionStorage.getItem("headerFooterBarVisible") || "false";

  if(headerFooterBarVisible === "true"){
    // HIDE HEADER BAR
    // console.log("HIDE Header Navigation Bar");
    // change button look
    $HeaderNavigationBarButton.removeClass("HeaderNavigationBarButtonUp");
    $HeaderNavigationBarButton.addClass("HeaderNavigationBarButtonDown");
    $FooterNavigationBarButton.removeClass("FooterNavigationBarButtonDown");
    $FooterNavigationBarButton.addClass("FooterNavigationBarButtonUp");

    // hide header bar hideHeaderNavigationBar
    $HeaderNavigationBar.removeClass();
    $HeaderNavigationBar.addClass("HeaderNavigationBar");
    $HeaderNavigationBar.addClass("HideHeaderNavigationBar");

    $FooterNavigationBar.removeClass();
    $FooterNavigationBar.addClass("FooterNavigationBar");
    $FooterNavigationBar.addClass("HideFooterNavigationBar");

    sessionStorage.setItem("headerFooterBarVisible", "false");
  }
}

/**
 * @name showBar
 * @description It shows the header and the footer simoultanesly.
 * @function
 * @public
 */
function showBar() {
  var headerFooterBarVisible = sessionStorage.getItem("headerFooterBarVisible") || "false";

  if(headerFooterBarVisible === "false"){
    // SHOW HEADER BAR
    // console.log("SHOW Header Navigation Bar");
    // change button look
    $HeaderNavigationBarButton.removeClass("HeaderNavigationBarButtonDown");
    $HeaderNavigationBarButton.addClass("HeaderNavigationBarButtonUp");

    $FooterNavigationBarButton.removeClass("FooterNavigationBarButtonUp");
    $FooterNavigationBarButton.addClass("FooterNavigationBarButtonDown");

    // show header bar showHeaderNavigationBar
    $HeaderNavigationBar.removeClass();
    $HeaderNavigationBar.addClass("HeaderNavigationBar");
    $HeaderNavigationBar.addClass("ShowHeaderNavigationBar");

    $FooterNavigationBar.removeClass();
    $FooterNavigationBar.addClass("FooterNavigationBar");
    $FooterNavigationBar.addClass("ShowFooterNavigationBar");

    sessionStorage.setItem("headerFooterBarVisible", "true");
  }
}

// Public ----------------------------------------------------------------------
/**
 * @name initModule
 * @description Initialization module called on page loaded. It sets the on(click) event for the header/footer bar buttons.
 * @function
 * @public
 */
headerFooterBar.initModule = function(){
  sessionStorage.removeItem('headerFooterBarVisible');
  $HeaderNavigationBarButtonInput.on("click", showHideHeaderFooterNavigationBar);
  $FooterNavigationBarButtonInput.on("click", showHideHeaderFooterNavigationBar);

  // HIDE/SHOW delle barre al click sul tasto destro del mouse.
  // if (document.addEventListener) { // IE >= 9; other browsers
  //     document.addEventListener('contextmenu', function(e) {
  //         // console.log("RIGHT CLICK");
  //         showHideHeaderFooterNavigationBar();
  //         e.preventDefault();
  //     }, false);
  // } else { // IE < 9
  //     document.attachEvent('oncontextmenu', function() {
  //         // console.log("RIGHT CLICK IE < 9");
  //         showHideHeaderFooterNavigationBar();
  //         window.event.returnValue = false;
  //     });
  // }

    let navigationWidth = $('.navigationbar-container').width();
    let iconNumber = $('.navbaricondynamic').length;
    if ((iconNumber * 92) > navigationWidth){
        $(".navigationbar").mCustomScrollbar({
            axis:"x", // horizontal scrollbar
            theme: "minimal",
            advanced:{autoExpandHorizontalScroll:true}
        });
    }


};

headerFooterBar.addButtonToFooterBar = addButtonToFooterBar;
headerFooterBar.addButtonToHeaderBar = addButtonToHeaderBar;
headerFooterBar.removeButtonFromBar = removeButtonFromBar;
headerFooterBar.hide = hideBar;
headerFooterBar.show = showBar;

module.exports = headerFooterBar;
