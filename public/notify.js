/**
 * Libreria per gestire le notifiche toast.
 *
 * @author Roberto Belardo <roberto.belardo@rai.it>
 * @version 1.0.0
 */

let notify = {};

// Error codes -----------------------------------------------------------------
notify.errors = {};
notify.errors.err_1000 = {"errCode": "1000", "errMessage": "Errore nella comunicazione con il server."};
notify.errors.err_1001 = {"errCode": "1001", "errMessage": "Il server ha risposto con un errore."};

notify.warnings = {};
// notify.warnings.warn_1000 = {"code": "1000", "message": ""};
// notify.warnings.warn_1001 = {"code": "1001", "message": "Num. Cespite SAP già presente sul sistema!"};
// // 4xxx errors relate to the frontend application
// errorsManager.errors.err_4000 = {errCode: "4000", errMessage: "Generic error in the frontend application."};
// errorsManager.errors.err_4001 = {errCode: "4001", errMessage: "Empty body in rest call from browser."};
// errorsManager.errors.err_4002 = {errCode: "4002", errMessage: "Validation error in rest call from browser."};
//
// // 5xxx errors related to backend services
// errorsManager.errors.err_5000 = {errCode: "5000", errMessage: "Generic error calling a backend service."};
// errorsManager.errors.err_5001 = {errCode: "5001", errMessage: "Backend service 'getUserTiles' returned an error."};
// errorsManager.errors.err_5002 = {errCode: "5002", errMessage: "Backend service 'getAssetTypes' returned an error."};
// errorsManager.errors.err_5003 = {errCode: "5003", errMessage: "Backend service 'getTicket' returned an error."};
// errorsManager.errors.err_5004 = {errCode: "5004", errMessage: "Backend service 'findContract' returned an error."};
// errorsManager.errors.err_5005 = {errCode: "5005", errMessage: "Backend service 'deliverAsset' returned an error."};

function notifyMultipleErrors(validation){
  for(let i=0; i < validation.errors.length; i++){
    let err = validation.errors[i];
    if (err) {
      notifyError(err.errorMessage);
    }
  }
}

function notifyError(message){
  notifyGeneric(message, "ATTENZIONE", "danger", "fa fa-times-circle");
}

function notifyWarning(message){
  notifyGeneric(message, "WARNING", "warning", "fa fa-exclamation-triangle");
}

function notifySuccess(message){
  notifyGeneric(message, "COMPLETATO", "success", "fa fa-check");
}

function notifyGeneric(message, title, type, icon, additionalOptions){

  let options = {};
  if(additionalOptions){
    options = additionalOptions;
  }

  options.type = "notify-"+type;
  options.animate = {
    "enter": "animated bounceInDown",
    "exit": "animated bounceOutUp"
  };

  options.template = '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
		'<span data-notify="icon"></span>' +
    '<span data-notify="title">{1}</span>' +
		'<span data-notify="message">{2}</span>' +
    '</div>'

  $.notify({
    "icon": icon,
    "title": "<strong>"+title+"</strong>",
    "message": message
  },options);
}

function searchSuccessFlashMessages(){
  let $li = $('#successFlashMessages');
  if($li){
    let $liList = $li.children();
    for (let i = 0; i < $liList.length; i++) {
      notifySuccess($liList[i].innerText);
    }
  }
}

function searchWarningFlashMessages(){
  let $li = $('#warningFlashMessages');
  if($li){
    let $liList = $li.children();
    for (let i = 0; i < $liList.length; i++) {
      notifyWarning($liList[i].innerText);
    }
  }
}


function searchErrorFlashMessages(){
  let $li = $('#errorFlashMessages');
  if($li){
    let $liList = $li.children();
    for (let i = 0; i < $liList.length; i++) {
      notifyError($liList[i].innerText);
    }
  }
}


function searchFlashMessages(){
  searchSuccessFlashMessages();
  searchWarningFlashMessages();
  searchErrorFlashMessages();
}

// Public ----------------------------------------------------------------------
notify.notifyMultipleErrors = notifyMultipleErrors;
notify.notifyError = notifyError;
notify.notifyWarning = notifyWarning;
notify.notifySuccess = notifySuccess;
notify.searchFlashMessages = searchFlashMessages;

module.exports = notify;
