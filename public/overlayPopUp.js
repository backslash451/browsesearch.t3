/**
 * OverlayPopUp Client module. Store functions used to manage the overlayPopUp.
 *
 * @module client/overlayPopUp
 * @author Roberto Belardo <roberto.belardo@rai.it>
 * @version 1.0.0
 */

var overlayPopUp = {};

// Private ---------------------------------------------------------------------
var  $fullScreenOverlayPopUp = $('#fullScreenOverlayPopUp'),
     $fullScreenOverlayPopUp_title =  $('#fullScreenOverlayPopUp_title'),
     $dismissOverlayPopUpButton = $('#fullScreenOverlayPopUp_dismissButton'),
     $fullScreenOverlayPopUp_message = $('#fullScreenOverlayPopUp_message'),
     $fullScreenOverlayPopUp_copyButton = $('#fullScreenOverlayPopUp_copyButton'),
     $fullScreenOverlayPopUp_content_icon = $('#fullScreenOverlayPopUp_content_icon');

/**
 * @name genericError
 * @description It shows the PopUp with a generic error message.
 * @function
 * @public
 *
 * @param {object} error - A value object holding the error title, messageTitle and messageBody
 */
function genericError(error) {
  showPopUp({
    "title": error.title || "ERROR 500",
    "icon": {
      "iconClass": "fa-exclamation-triangle",
      "color": "red"
    },
    "messageHeader": error.messageTitle || "Internal Server Error",
    "messageBody": [{"pContent": error.messageBody || "Errore server. Contattare un amministratore per ulteriori dettagli."}],
  });
}

/**
 * @name handleClickOnDismissOverlayPopUpButton
 * @description It handles the dismiss of the PopUp
 * @function
 * @public
 */
function handleClickOnDismissOverlayPopUpButton() {
  $fullScreenOverlayPopUp.hide();
  // $fullScreenOverlayPopUp_copyButton.show();
  $('#fullScreenOverlayPopUp_copyButton').tooltip('dispose');

  // Destroy additional control buttons such as openLink or copy
  $('.fullScreenOverlayPopUp_controls_button').remove();
}

/**
 * @name showPopUp
 * @description It shows the PopUp.
 * @function
 * @public
 *
 * @param {object} properties - A value object holding all the PopUp properties like title, icon, color etc...
 */
function showPopUp(properties) {
  // Example properties object
  // {
  //   title: "Identificativo di consegna generato con successo" || "404" || "Ticket generato con success",
  //   icon: {
  //     iconClass: "fa-exclamation-triangle",
  //     color: "black"
  //   },
  //   messageHeader: "C12345678 - 098765" || "Page not found",
  //   messageBody: [
  //     {pContent: "La pagina richiesta non esiste"},
  //     {pContent: "E' stata inviata una email al tuo indirizzo di posta elettronica con questo ticket"},
  //     {pContent: "L'indentificativo di consegna  è stato correttamente associato al seguente materiale"},
  //     {pContent: "55555 - Il trono di spade ep 12, Season 1"},
  //     {
  //      pContent: "41ee3545-556d-468f-bb35-ab6b597f8297",
  //      divID: "copyID"
  //     }
  //   ],
  //   controls: {
  //     copyButton: {
  //      divID: "copyID"
  //     },
  //     openLink: {
  //      url: "http://www.google.it",
  //      label: "GOOGLE"
  //     },
  //   }
  // }

  // console.log(properties.title);

  $fullScreenOverlayPopUp_title.html(properties.title);
  $fullScreenOverlayPopUp_message.children(".fullScreenOverlayPopUp_message_title").html(properties.messageHeader);

  // Icon
  $fullScreenOverlayPopUp_content_icon.empty();
  if(properties.icon){
    $fullScreenOverlayPopUp_content_icon.append("<i class='fa "+properties.icon.iconClass+" fa-2x' style='color:"+properties.icon.color+";' aria-hidden='true'></i>");
  }else{
    $fullScreenOverlayPopUp_content_icon.append("<i class='fa fa-info-circle fa-2x' aria-hidden='true'></i>");
  }

  // MessageBody
  var $messageBody = $fullScreenOverlayPopUp_message.children(".fullScreenOverlayPopUp_message_body");
  $messageBody.empty();
  if(properties.messageBody){

    // FIX for IE 11 e minor versions
    // for(element_idx in elements) {
    //  element = elements[element_idx];

    for (let paragraph_idx in properties.messageBody) {
      if(properties.messageBody[paragraph_idx].divID){
        $messageBody.append("<p id='"+properties.messageBody[paragraph_idx].divID+"' class='fullScreenOverlayPopUp_message_body_p'>"+properties.messageBody[paragraph_idx].pContent+"</p>");
      }
      else{
        $messageBody.append("<p class='fullScreenOverlayPopUp_message_body_p'>"+properties.messageBody[paragraph_idx].pContent+"</p>");
      }
    }
  }

  // Controls
  if (properties.controls) {
    if (properties.controls.copyButton) {
      $('#fullScreenOverlayPopUp_controls').prepend("<a id='fullScreenOverlayPopUp_copyButton' style='margin-right:5px;' class='btn bluButton fullScreenOverlayPopUp_controls_button' title='Copiato!' data-clipboard-target='#"+properties.controls.copyButton.divID+"' type='button' name='button'>Copia</a>");
      // $fullScreenOverlayPopUp_copyButton.show();
    }
    // else{
    //   $fullScreenOverlayPopUp_copyButton.hide();
    // }

    if (properties.controls.openLink) {
      $('#fullScreenOverlayPopUp_controls').prepend("<a href='"+properties.controls.openLink.url+"' target='_blank' style='margin-right:5px;' class='btn bluButton fullScreenOverlayPopUp_controls_button'>"+properties.controls.openLink.label+"</a>");
    }
  }

  $fullScreenOverlayPopUp.show();

}

// Public ----------------------------------------------------------------------
/**
 * @name initModule
 * @description Initialization module called on page loaded. It sets the on(click) event to dismiss the overlayPopUp.
 * @function
 * @public
 */
overlayPopUp.initModule = function(){
  $dismissOverlayPopUpButton.on("click", handleClickOnDismissOverlayPopUpButton);
};

overlayPopUp.showPopUp = showPopUp;
overlayPopUp.genericError = genericError;
overlayPopUp.hidePopUp = handleClickOnDismissOverlayPopUpButton;

module.exports = overlayPopUp;
