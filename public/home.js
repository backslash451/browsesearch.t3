let home = {};

let notify = require('./notify.js');
let bodyRequest;
let pageNumberElement = 15;
let filterForApiUrl;

// Private ---------------------------------------------------------------------
let getFormat = function () {
  let body = {
    "key": "VIDEOFORMATCODE"
  };

  let call = $.post({
    data: body,
    url: "/api/getTopology",
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.has_error) {
        console.log("Errore chiamata")
      } else {
        let formatSelect = response.result.TopologyValues;
        if (!formatSelect) {
          console.log("FormatSelect vuoto")
        } else if (formatSelect) {
          for (var i = 0; i < formatSelect.length; i++) {
            $('#pf-formato, #cf-formato').append(
              '<option id="' + formatSelect[i].Id + '" value="' + formatSelect[i].Name + '">' + formatSelect[i].Name + '</option>'
            )
          }
        }
      }
    });
}

let getGenre = function () {
  let body = {
    "key": 'CONTENTGENRE',
  };

  let call = $.post({
    data: body,
    url: "/api/getTopology",
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.has_error) {

      } else {
        let genreSelect = response.result.TopologyValues;
        if (!genreSelect) {

        } else if (genreSelect) {
          for (var i = 0; i < genreSelect.length; i++) {
            $('#pf-genere').append(
              '<option id="' + genreSelect[i].Id + '" value="' + genreSelect[i].Name + '">' + genreSelect[i].Name + '</option>'
            )
          }
        }
      }
    });
}

let getAspectRatio = function () {
  let body = {
    "key": 'VISUALASPECTRATIO',
  };

  let call = $.post({
    data: body,
    url: "/api/getTopology",
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.has_error) {

      } else {
        let aspectRatio = response.result.TopologyValues;
        if (!aspectRatio) {

        } else if (aspectRatio) {
          for (var i = 0; i < aspectRatio.length; i++) {
            $('#pf-aspect-ratio, #cf-aspect-ratio').append(
              '<option id="' + aspectRatio[i].Id + '" value="' + aspectRatio[i].Name + '">' + aspectRatio[i].Name + '</option>'
            )
          }
        }
      }
    });
}

let getTelSystem = function () {
  let body = {
    "key": 'TELEVISIONSYSTEM',
  };

  let call = $.post({
    data: body,
    url: "/api/getTopology",
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.has_error) {

      } else {
        let standartSelect = response.result.TopologyValues;
        if (!standartSelect) {

        } else if (standartSelect) {
          for (var i = 0; i < standartSelect.length; i++) {
            $('#pf-standard, #cf-standard').append(
              '<option id="' + standartSelect[i].id + '" value="' + standartSelect[i].Name + '">' + standartSelect[i].Name + '</option>'
            )
          }
        }
      }
    });
}

let getContentVersion = function () {
  let body = {
    "key": 'CONTENTVERSION',
  };

  let call = $.post({
    data: body,
    url: "/api/getTopology",
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.has_error) {

      } else {
        let contentVersion = response.result.TopologyValues;
        if (!contentVersion) {

        } else if (contentVersion) {
          for (var i = 0; i < contentVersion.length; i++) {
            $('#cf-categoria').append(
              '<option id="' + contentVersion[i].id + '" value="' + contentVersion[i].Name + '">' + contentVersion[i].Name + '</option>'
            )
          } 
        }
      }
    });
}

let getChannelList = function () {

  let call = $.post({
    url: "/api/getChannelList",
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.has_error) {

      } else {
        let channelList = response.result.ChannelList;
        if (!channelList) {

        } else if (channelList) {
          for (var i = 0; i < channelList.length; i++) {
            $('#ep-canale, #og-canale').append(
              '<option id="' + channelList[i].IdChannel + '" value="' + channelList[i].ChannelName + '">' + channelList[i].ChannelName + '</option>'
            )
          } 
        }
      }
    });
}

// Public ----------------------------------------------------------------------
home.startSearch = function (e) {
  // la variabile filtroRicerca sarà popolata con (searchEditorialVersion-searchGraphicContent-searchPlaylistItem)
  let filtroRicerca = $('.tabs a.active').attr('data-id');

  if (filtroRicerca == "fields-pf") {
    filterForApiUrl = "searchEditorialVersion"
    bodyRequest = {
      "EditorialVersionCriteria": {
        "IsProgram": true,
        "FreeText": $('#free-text').val() == "" ? null : $('#free-text').val(),
        "Range": {
          "from": 0,
          "to": pageNumberElement
        },
        "Program": $('#pf-titolo-programma').val() == "" ? null : $('#pf-titolo-programma').val(),
        "Episode": $('#pf-titolo-episodio').val() == "" ? null : $('#pf-titolo-episodio').val(),
        "Uorg": $('#pf-uorg').val() == "" ? null : $('#pf-uorg').val(),
        "CorporateSerial": $('#pf-matricola').val() == "" ? null : $('#pf-matricola').val(),
        "PlayoutId": $('#pf-playout-id').val() == "" ? null : $('#pf-playout-id').val(),
        "Barcode": $('#pf-barcode').val() == "" ? null : $('#pf-barcode').val(),
        "FriendlyCode": $('#pf-friendly-code').val() == "" ? null : $('#pf-friendly-code').val(),
        "IdTeca": $('#pf-id-teca').val() == "" ? null : $('#pf-id-teca').val(),
        "EditorialVersionTitle": $('#pf-titolo-versione-editoriale').val() == "" ? null : $('#pf-titolo-versione-editoriale').val(),
        "VideoFormatCode": {
          "Id": $('#pf-formato option:selected').prop("value") == "" ? null : $('#pf-formato option:selected').prop("id"),
          "Name": $('#pf-formato option:selected').prop("value") == "" ? null : $('#pf-formato option:selected').prop("value"),
          "Description": $('#pf-formato option:selected').prop("value") == "" ? null : $('#pf-formato option:selected').prop("value")
        },
        "ContentGenre": {
          "Id": $('#pf-genre option:selected').prop("value") == "" ? null : $('#pf-genere option:selected').prop("id"),
          "Name": $('#pf-genre option:selected').prop("value") == "" ? null : $('#pf-genere option:selected').prop("value"),
          "Description": $('#pf-genre option:selected').prop("value") == "" ? null : $('#pf-genere option:selected').prop("value")
        },
        "VisualAspectRatio": {
          "Id": $('#pf-aspect-ratio option:selected').prop("value") == "" ? null : $('#pf-aspect-ratio option:selected').prop("id"),
          "Name": $('#pf-aspect-ratio option:selected').prop("value") == "" ? null : $('#pf-aspect-ratio option:selected').prop("value"),
          "Description": $('#pf-aspect-ratio option:selected').prop("value") == "" ? null : $('#pf-aspect-ratio option:selected').prop("value")
        },
        "TelevisionSystem": {
          "Id": $('#pf-standard option:selected').prop("value") == "" ? null : $('#pf-standard option:selected').prop("id"),
          "Name": $('#pf-standard option:selected').prop("value") == "" ? null : $('#pf-standard option:selected').prop("value"),
          "Description": $('#pf-standard option:selected').prop("value") == "" ? null : $('#pf-standard option:selected').prop("value")
        },
        "AudioDescription": $('#pf-audio-descrizione').is(":checked"),
        "Subtitling": $('#pf-sottotitoli').is(":checked"),
        "ClearSubtitling": $('#pf-televideo').is(":checked"),
        "Signing": $('#pf-linguaggio-gesti').is(":checked"),
        "EpisodeNumber": $('#pf-numero-episodio').val() == "" ? null : $('#pf-numero-episodio').val(),
        "OriginalEpisodeNumber": $('#pf-numero-episodio-rai').val() == "" ? null : $('#pf-numero-episodio-rai').val(),
        "GuidProduct": $('#pf-id-prodotto-aspid').val() == "" ? null : $('#pf-id-prodotto-aspid').val(),
        "GuidEditorial": $('#pf-id-editoriale-aspid').val() == "" ? null : $('#pf-id-editoriale-aspid').is(":checked")
      }
    }
  }
  else if (filtroRicerca == "fields-cf") {
    filterForApiUrl = "searchGraphicContent"
    bodyRequest = {
      "EditorialVersionCriteria": {
        "IsProgram": false,
        "FreeText": $('#free-text').val() == "" ? null : $('#free-text').val(),
        "Range": {
          "from": 0,
          "to": pageNumberElement
        },
        "Program": $('#cf-titolo-programma').val() == "" ? null : $('#cf-titolo-programma').val(),
        "Episode": $('#cf-titolo-episodio').val() == "" ? null : $('#cf-titolo-episodio').val(),
        "Uorg": $('#cf-uorg').val() == "" ? null : $('#cf-uorg').val(),
        "CorporateSerial": $('#cf-matricola').val() == "" ? null : $('#cf-matricola').val(),
        "PlayoutId": $('#cf-playout-id').val() == "" ? null : $('#cf-playout-id').val(),
        "Barcode": $('#cf-barcode').val() == "" ? null : $('#cf-barcode').val(),
        "ContributionTitle": "String",
        "FriendlyCode": $('#cf-friendly-code').val() == "" ? null : $('#cf-friendly-code').val(),
        "IdTeca": $('#cf-id-teca').val() == "" ? null : $('#cf-id-teca').val(),
        "EditorialVersionTitle": $('#cf-titolo-versione-editoriale').val() == "" ? null : $('#cf-titolo-versione-editoriale').val(),
        "VideoFormatCode": {
          "Id": $('#cf-formato option:selected').prop("value") == "" ? null : $('#cf-formato option:selected').prop("id"),
          "Name": $('#cf-formato option:selected').prop("value") == "" ? null : $('#cf-formato option:selected').prop("value"),
          "Description": $('#cf-formato option:selected').prop("value") == "" ? null : $('#cf-formato option:selected').prop("value")
        },
        "VisualAspectRatio": {
          "Id": $('#cf-aspect-ratio option:selected').prop("value") == "" ? null : $('#cf-aspect-ratio option:selected').prop("id"),
          "Name": $('#cf-aspect-ratio option:selected').prop("value") == "" ? null : $('#cf-aspect-ratio option:selected').prop("value"),
          "Description": $('#cf-aspect-ratio option:selected').prop("value") == "" ? null : $('#cf-aspect-ratio option:selected').prop("value")
        },
        "TelevisionSystem": {
          "Id": $('#cf-standard option:selected').prop("value") == "" ? null : $('#cf-standard option:selected').prop("id"),
          "Name": $('#cf-standard option:selected').prop("value") == "" ? null : $('#cf-standard option:selected').prop("value"),
          "Description": $('#cf-standard option:selected').prop("value") == "" ? null : $('#cf-standard option:selected').prop("value")
        },
        "ContentGenre": {
          "Id": $('#cf-categoria option:selected').prop("value") == "" ? null : $('#cf-categoria option:selected').prop("id"),
          "Name": $('#cf-categoria option:selected').prop("value") == "" ? null : $('#cf-categoria option:selected').prop("value"),
          "Description": $('#cf-categoria option:selected').prop("value") == "" ? null : $('#cf-categoria option:selected').prop("value")
        },
        "SubFunctionalarea": {
          "Id": $('#cf-redazione option:selected').prop("value") == "" ? null : $('#cf-redazione option:selected').prop("id"),
          "Name": $('#cf-redazione option:selected').prop("value") == "" ? null : $('#cf-redazione option:selected').prop("value"),
          "Description": $('#cf-redazione option:selected').prop("value") == "" ? null : $('#cf-redazione option:selected').prop("value")
        },
        "AudioDescription": $('#cf-audio-descrizione').is(":checked"),
        "Subtitling": $('#cf-sottotitoli').is(":checked"),
        "ClearSubtitling": $('#cf-televideo').is(":checked"),
        "Signing": $('#cf-linguaggio-gesti').is(":checked"),
        "EpisodeNumber": $('#cf-numero-episodio').val() == "" ? null : $('#cf-numero-episodio').val(),
        "OriginalEpisodeNumber": $('#cf-numero-episodio-rai').val() == "" ? null : $('#cf-numero-episodio-rai').val(),
        "GuidProduct": $('#cf-id-prodotto-aspid').val() == "" ? null : $('#cf-id-prodotto-aspid').val(),
        "GuidEditorial": $('#cf-id-editoriale-aspid').val() == "" ? null : $('#cf-id-editoriale-aspid').is(":checked")
      }
    }
  } else if (filtroRicerca == "fields-og") {
    filterForApiUrl = "searchGraphicContent"
    bodyRequest = {
      "GraphicContentCriteria": {
        "ChannelId": $('#og-id-canale').val() == "" ? null : $('#og-id-canale').val(),
        "GraphicContentType": $('#og-contenuto-grafico').val() == "" ? null : $('#og-contenuto-grafico').val(),
        "FreeText": $('#free-text').val() == "" ? null : $('#free-text').val(),
        "Range": {
          "from": 0,
          "to": pageNumberElement
        }
      }
    }
  } else if (filtroRicerca == "fields-ep") {
    filterForApiUrl = "searchPlaylistItem"
    bodyRequest = {
      "PlaylistItemCriteria": {
        "FreeText": $('#free-text').val() == "" ? null : $('#free-text').val(),
        "Range": {
          "from": 0,
          "to": pageNumberElement
        },
        "GroupByEvent": true,
        "MO": {
          "Channel": $('#ep-canale option:selected').prop("value") == "" ? null : $('#ep-canale option:selected').prop("value"),
          "Date": $('#ep-data').val(),
          "PlayoutId": $('#ep-playout-id').val() == "" ? null : $('#ep-playout-id').val()
        },
        "Barcode": $('#ep-barcode').val() == "" ? null : $('#ep-barcode').val(),
        "PlaylistItemType": $('#ep-tipologia option:selected').prop("value") == "" ? null : $('#ep-tipologia option:selected').prop("value"),
        "GuidProduct": $('#ep-id-prodotto-aspid').val() == "" ? null : $('#ep-id-prodotto-aspid').val(),
        "GuidEditorial": $('#ep-id-prodotto-aspid').val() == "" ? null : $('#ep-id-prodotto-aspid').val()
      }
    }
  }

  console.log("Body Request : " + JSON.stringify(bodyRequest))

  let finalBodyRequest = bodyRequest;

  let call = $.post("/api/" + filterForApiUrl, {
    "data": JSON.stringify(finalBodyRequest)
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.result.has_error) {

      } else {
        let searchResult = response.result;
        if (!searchResult) {

        } else if (searchResult) {
          let resultList = searchResult.EditorialVersions;
          //popoliamo lista risultati ricerca.
          //Aggiungere controllo per searchResult. --> in base al filtro avrà una proprietà diversa (es. EditorialVersion, GraphicContentCriteria etc...)
        }
      }
    });
}

home.showMore = function () {
  let actualRangeFrom;
  let actualRangeTo;
  if (bodyRequest.hasOwnProperty('EditorialVersionCriteria')) {
    actualRageFrom = bodyRequest.EditorialVersionCriteria.Range.from;
    actualRageTo = bodyRequest.EditorialVersionCriteria.Range.to;
    bodyRequest.EditorialVersionCriteria.Range.from = actualRangeFrom + pageNumberElement;
    bodyRequest.EditorialVersionCriteria.Range.to = actualRangeTo + pageNumberElement;
  } else if (bodyRequest.hasOwnProperty('GraphicContentCriteria')) {
    actualRageFrom = bodyRequest.GraphicContentCriteria.Range.from;
    actualRageTo = bodyRequest.GraphicContentCriteria.Range.to;
    bodyRequest.GraphicContentCriteria.Range.from = actualRangeFrom + pageNumberElement;
    bodyRequest.GraphicContentCriteria.Range.to = actualRangeTo + pageNumberElement;
  } else if (bodyRequest.hasOwnProperty('PlaylistItemCriteria')) {
    actualRageFrom = bodyRequest.PlaylistItemCriteria.Range.from;
    actualRageTo = bodyRequest.PlaylistItemCriteria.Range.to;
    bodyRequest.PlaylistItemCriteria.Range.from = actualRangeFrom + pageNumberElement;
    bodyRequest.PlaylistItemCriteria.Range.to = actualRangeTo + pageNumberElement;
  }

  let finalBodyRequest = bodyRequest;

  let call = $.post("/api/" + filterForApiUrl, {
    "data": JSON.stringify(finalBodyRequest)
  });

  call
    .fail(function (err) {
      overlayPopUp.genericError();
    })
    .done(function (response) {
      if (response.result.has_error) {

      } else {
        let searchResult = response.result;
        if (!searchResult) {

        } else if (searchResult) {
          let resultList = searchResult.EditorialVersions;
          //popoliamo lista risultati ricerca pagina successiva
        }
      }
    });
}

home.initModule = function () {
  $('#searchbar').submit(false);
  notify.searchFlashMessages();

  getFormat();
  getGenre();
  getAspectRatio();
  getTelSystem();
  getContentVersion();
  getChannelList();

  $('.tabs a').on('click', function (e) {
    e.preventDefault();

    let filtersId = $(this).data('id');
    $('#' + filtersId + ', [data-id=' + filtersId + ']').addClass('active').siblings().removeClass('active');
  });

};

module.exports = home;