let browsesearch_t3 = browsesearch_t3 || {}; // Global App NameSpace

browsesearch_t3.home = require('./home.js');

browsesearch_t3.headerFooterBar = require('./headerFooterBar.js');
browsesearch_t3.notify = require('./notify.js');
browsesearch_t3.overlayPopUp = require('./overlayPopUp.js');

// browsesearch_t3.templates = require('./templates.js');
browsesearch_t3.partials = require('./partials.js');
browsesearch_t3.helpers = require('./helpers.js');

module.exports = browsesearch_t3;
