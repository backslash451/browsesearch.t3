function hello() {
  return "Hello World";
}

function toUpperCase(str) {
  if(str && typeof str === "string") {
    return str.toUpperCase();
  }
  return str;
}

// GLOBAL HANDLEBARS HELPERS
let register = function(Handlebars) {

    let helpers = {
        // put all of your helpers inside this object
        "hello": hello,
        "toUpperCase": toUpperCase
    };

    if (Handlebars && typeof Handlebars.registerHelper === "function") {
        // register helpers
        for (let prop in helpers) {
            Handlebars.registerHelper(prop, helpers[prop]);
        }
    } else {
        // just return helpers object if we can't register helpers here
        return helpers;
    }

};

// client
if (typeof window !== "undefined") {
    register(Handlebars);
}
// server
else {
    module.exports.register = register;
    module.exports.helpers = register(null);
}
