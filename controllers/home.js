let express = require('express');
let routerPrivate = express.Router();

let axios = require('axios');

let logger = require('../lib/logger'),
  errMgt = require('../lib/errorsManager'),
  auth = require('../lib/auth');

let env = (function () {
  let Habitat = require("habitat");
  Habitat.load();
  return new Habitat();
}());

// Auth middleware for private routes ------------------------------------------
routerPrivate.use(function (req, res, next) {
  auth.isLoggedIn(req, res, next);
}, function (req, res, next) {
  auth.authMiddleware(req, res, next);
});

// ROUTES ----------------------------------------------------------------------
routerPrivate.get('/', function (req, res) {
  logger.info("GET /");

  req.session.browseSearch = {};

  res.render('home', {
    "headerTitle": "Home",
    "user": req.session.userDisplayName,
    "flashes": req.flash()
  });
});


// API -------------------------------------------------------------------------

// INIZIO API PER POPOLARE SELECT
routerPrivate.post('/api/getTopology', function (req, res) {
  logger.info("GET /getTopology");
  let filterType = req.body.key;
  logger.info("GET /getTopology --> filterType : " + filterType);
  axios.post(env.get('BUSINESS_SERVICES').get_topology, {
    "IdGUIUser": "123456789",
    "Topology": filterType,
  })

    .then(function (response) {
      if (response.data.has_error) {
        throw new Error();
      } else {
        res.json({
          "result": response.data
        });
      }
    })

    .catch(function (err) {
      logger.error(err);
      errMgt.handleError500(res, true, errMgt.getErrorCodeAndMessage(5000));
    })

});

routerPrivate.post('/api/getChannelList', function (req, res) {
  logger.info("GET /getChannelList");
  if (req.session.browseSearch.listaCanali) {
    res.json({
      "result": req.session.browseSearch.listaCanali
    });
  } else {
    axios.post(env.get('BUSINESS_SERVICES').get_channel_list, {
      "IdGUIUser": "123456789"
    })

      .then(function (response) {
        if (response.data.has_error) {
          throw new Error();
        } else {
          let listaCanali = response.data;
          res.json({
            "result": listaCanali
          });
        }
      })

      .catch(function (err) {
        logger.error(err);
        errMgt.handleError500(res, true, errMgt.getErrorCodeAndMessage(5000));
      })
  }
});
// FINE API PER POPOLARE SELECT

// INIZIO API PER EFFETTUARE RICERCA
routerPrivate.post('/api/searchEditorialVersion', function (req, res) {
  logger.info("GET /searchEditorialVersion + req.body");
  let prodOrContribute = JSON.parse(req.body.data);

  logger.info("GET /searchEditorialVersion --> Body Chiamata di ricerca : " + JSON.stringify(prodOrContribute));
  axios.post(env.get('BUSINESS_SERVICES').search_editorial_version, prodOrContribute)

    .then(function (response) {
      if (response.data.has_error) {
        throw new Error();
      } else {
        let searchResult = response.data;
        res.json({
          "result": searchResult
        });
      }
    })

    .catch(function (err) {
      logger.error(err);
      errMgt.handleError500(res, true, errMgt.getErrorCodeAndMessage(5000));
    })
});

routerPrivate.post('/api/searchGraphicContent', function (req, res) {
  logger.info("GET /searchGraphicContent");
  let graphObject = JSON.parse(req.body.data);
  graphObject.IdGUIUser = req.session.t3mfIdGUIUser;

  axios.post(env.get('BUSINESS_SERVICES').search_graphic_content, graphObject)

    .then(function (response) {
      if (response.data.has_error) {
        throw new Error();
      } else {
        let searchResult = response.data;
        res.json({
          "result": searchResult
        });
      }
    })

    .catch(function (err) {
      logger.error(err);
      errMgt.handleError500(res, true, errMgt.getErrorCodeAndMessage(5000));
    })
});

routerPrivate.post('/api/searchPlaylistItem', function (req, res) {
  logger.info("GET /searchPlaylistItem");
  let listItem = JSON.parse(req.body.data);
  listItem.IdGUIUser = req.session.t3mfIdGUIUser;

  axios.post(env.get('BUSINESS_SERVICES').search_playlist_item, listItem)

    .then(function (response) {
      if (response.data.has_error) {
        throw new Error();
      } else {
        let searchResult = response.data;
        res.json({
          "result": searchResult
        });
      }
    })

    .catch(function (err) {
      logger.error(err);
      errMgt.handleError500(res, true, errMgt.getErrorCodeAndMessage(5000));
    })
});
// FINE API PER EFFETTUARE RICERCA

// EXPORT ----------------------------------------------------------------------
module.exports = {
  // public: routerPublic,
  private: routerPrivate
};
