const webpack = require('webpack');
const path = require('path');
const pjson = require('./package.json');
const WebpackShellPlugin = require('webpack-shell-plugin');


// ['./public/js/common.js', './public/js/moveTo.js', './public/js/test.js'],
module.exports = {
  entry: './public/browsesearch_t3.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle_'+pjson.version+'.min.js',
    library: ["browsesearch_t3", "[name]"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      }
    ]
  },
  plugins: process.env.NODE_ENV === 'production' ? [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /it/),
    new WebpackShellPlugin({
      safe: true,
      onBuildStart:[
        'echo "<script src=\'/bundle_'+pjson.version+'.min.js\' type=\'text/javascript\'></script>" > views/partials/layout/bundle.handlebars'
      ]
    })
  ] : [
    new WebpackShellPlugin({
      safe: true,
      onBuildStart:[
        'echo "<script src=\'/bundle_'+pjson.version+'.min.js\' type=\'text/javascript\'></script>" > views/partials/layout/bundle.handlebars'
      ]
    })
  ]
}
