/**
 * Libreria per gestire il log che wrappa winston.
 *
 * @author Roberto Belardo <roberto.belardo@rai.it>
 * @version 1.0.0
 */

// let logger = {};

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

let env = (function(){
  let Habitat = require("habitat");
  Habitat.load();
  return new Habitat();
}());

const myFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});


let logger = createLogger({
  level: env.get("LOG_LEVEL"),
  format: format.combine(
    format.colorize(),
    format.json(),
    label({"label": env.get("APP_ID")}),
    timestamp(),
    myFormat
  ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    // new transports.File({ filename: 'error.log', level: 'error' }),
    // new transports.File({ filename: 'combined.log' }),
    new transports.Console()
  ]
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
// logger.add(new transports.Console({
//   format: format.simple()
// }));
// if (process.env.NODE_ENV !== 'production') {
//   logger.add(new transports.Console({
//     format: format.simple()
//   }));
// }

module.exports = logger;
