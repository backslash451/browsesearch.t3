/**
 * Libreria per gestire gli errori.
 *
 * @author Roberto Belardo <roberto.belardo@rai.it>
 * @version 1.0.0 (Versione modificata senza il supporto per i18next)
 */


let errorsManager = {};
let logger = require('./logger');

// Error codes -----------------------------------------------------------------
errorsManager.errors = {};
// 4xxx errors relate to the frontend application
errorsManager.errors.err_4000 = {errCode: "4000", errMessage: "Generic error in the frontend application."};
errorsManager.errors.err_4001 = {errCode: "4001", errMessage: "Empty body in rest call from browser."};
errorsManager.errors.err_4002 = {errCode: "4002", errMessage: "Validation error in rest call from browser."};

// 5xxx errors related to backend services
errorsManager.errors.err_5000 = {errCode: "5000", errMessage: "Generic error calling a backend service."};
errorsManager.errors.err_5001 = {errCode: "5001", errMessage: "Backend service 'getUserTiles' returned an error."};
errorsManager.errors.err_5002 = {errCode: "5002", errMessage: "Backend service 'getAssetTypes' returned an error."};
errorsManager.errors.err_5003 = {errCode: "5003", errMessage: "Backend service 'getTicket' returned an error."};
errorsManager.errors.err_5004 = {errCode: "5004", errMessage: "Backend service 'findContract' returned an error."};
errorsManager.errors.err_5005 = {errCode: "5005", errMessage: "Backend service 'deliverAsset' returned an error."};

// Error handlers --------------------------------------------------------------

/**
 * getErrorCodeAndMessage.
 * @description It build an error log screen composed of error code + error message as mapped above.
 * @name getErrorCodeAndMessage
 * @function
 * @param  {string} code - The error codes
 * @return {string}      - The error string message
 */
errorsManager.getErrorCodeAndMessage = function(code) {
  return "Error T3-"+errorsManager.errors["err_"+code].errCode+". "+errorsManager.errors["err_"+code].errMessage;
}

/**
  * Handle Error.
  * @description It handles the generic error rendering an error page if the error is not related to an api or sending the error data to the client via json otherwise.
  * @name handleError
  * @function
  * @private
  * @param {object} res - The Express Response object
  * @param {boolean} api - If the error to manage is related to an api or not
  * @return An error page if the error is not related to an api or sending the error data to the client via json otherwise.
  */
function handleError(res, api, errorData) {
  logger.error("ERROR: " + errorData.title + " - " + errorData.messageTitle + ": " + errorData.messageBody);

  if(api){
    return res.json(errorData);
  }else{
    return res.render("error", {
      "headerTitle": errorData.title,
      "displayFullScreenOverlayPopUp": true,
      "fullScreenOverlayPopUp_title": errorData.title,
      "fullScreenOverlayPopUp_message_title": errorData.messageTitle,
      "messageBody": [
        {"pContent": errorData.messageBody}
      ]
    });
  }
}

/**
  * Handle Error 400
  * @description It handles the HTTP 400 error.
  * @name handleError400
  * @function
  * @param {object} res - The Express Response object
  * @param {boolean} api - If the error to manage is related to an api or not
  * @return An error page if the error is not related to an api or sending the error data to the client via json otherwise.
  */
errorsManager.handleError400 = function(res, api) {
  var errorData = {
    "has_error": true,
    "title": "ERROR 400",
    "messageTitle": "Richiesta malformata",
    "messageBody": "Errore nella richiesta. Contattare un amministratore per ulteriori dettagli."
  };

  handleError(res, api, errorData);
};

/**
  * Handle Error 401
  * @description It handles the HTTP 401 error.
  * @name handleError401
  * @function
  * @param {object} res - The Express Response object
  * @param {boolean} api - If the error to manage is related to an api or not
  * @return An error page if the error is not related to an api or sending the error data to the client via json otherwise.
  */
errorsManager.handleError401 = function(res, api) {
  var errorData = {
    "has_error": true,
    "title": "ERROR 401",
    "messageTitle": "Non autorizzato",
    "messageBody": "Non si è abilitati per la risorsa richiesta. Contattare un amministratore per ulteriori dettagli."
  };

  handleError(res, api, errorData);
};

/**
  * Handle Error 500
  * @description It handles the HTTP 500 error.
  * @name handleError500
  * @function
  * @param {object} res - The Express Response object
  * @param {boolean} api - If the error to manage is related to an api or not
  * @param {string} error_message - The error message to show in the the messageBody div of the error page
  * @return An error page if the error is not related to an api or sending the error data to the client via json otherwise.
  */
errorsManager.handleError500 = function(res, api, error_message) {
  var errorData = {
    "has_error": true,
    "title": "ERROR 500",
    "messageTitle": "Errore interno del server. Contattare un amministratore per ulteriori dettagli.",
    "messageBody": error_message
  };

  handleError(res, api, errorData);
};

module.exports = errorsManager;
