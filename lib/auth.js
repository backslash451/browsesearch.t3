/**
 * Libreria per gestire l'autenticazione e l'autorizzazione.
 *
 * @author Roberto Belardo <roberto.belardo@rai.it>
 * @version 1.1.0
 */

let auth = {};

let logger = require('./logger'),
    errMgt = require('./errorsManager');

let env = (function(){
  let Habitat = require("habitat");
  Habitat.load();
  return new Habitat();
}());

/**
 * AuthMiddleware
 * @name authMiddleware
 * @description Middleware use to manage the authorization of the tiles.
 * @function
 * @param {object} req - The Express Reques object
 * @param {object} res - The Express Response object
 * @param {calback} next - The Express Next function
 * @param {const} _tileID - The id of the tile that needs to be verified
 */
auth.authMiddleware = function(req, res, next) {
  logger.debug("authMiddleware");
  var authorizedTiles = req.session.authorizedTiles;
  var auth = false;

  if(!authorizedTiles){
    logger.error("ERROR: Authentication failed while retrieving authorizedTiles.");
    return errMgt.handleError401(res);
  }

  for (tile of authorizedTiles) {
    if (tile.tileID === env.get("APP_ID")) {
      auth = true;
      break;
    }
  }

  if (auth) {
    logger.debug("Autorizzato -> next");
    next()
  } else {
    logger.error("ERROR: User not authorized for tile " + env.get("APP_ID"));
    return errMgt.handleError401(res);
  }
};

auth.isLoggedIn = function(req, res, next) {
    if (req.session.userId != null){
      logger.debug(req.session.userId +" is logged in!");
      return next();
    } else {
      if (req.xhr) {
        logger.debug("XHR > 403");
        req.flash("error", "Autenticazione scaduta. Per favore eseguire nuovamente il login.");
        res.status(403).json({
          "result": false,
          "error": 'You must login to see this',
          "location": '/auth/login' // TODO: Rendere coerente con l'app di authentication
        });
      } else {
        logger.debug("redirect to auth/login");
        res.redirect(env.get("AUTH_APP_URL"));
      }
    }
}

module.exports = auth;
